<?php
    class ImagenProducto {

        private $_idImagenProducto;
        private $_rutaImagenProducto;

        function get_idImagenProducto() {
            return $this->_idImagenProducto;
        }

        function set_idImagenProducto($idImagenProducto) {
            $this->_idImagenProducto = $idImagenProducto;
        }

        function get_rutaImagenProducto() {
            return $this->_rutaImagenProducto;
        }

        function set_rutaImagenProducto($rutaImagenProducto) {
            $this->_rutaImagenProducto = $rutaImagenProducto;
        }
    }
?>