<?php
    class CategoriaProducto {

        private $_idProducto;
        private $_idEmpresa;
        private $_idCategoriaPadre;
        private $_nombre;

        function get_idProducto() {
            return $this->_idProducto;
        }

        function set_idProducto($_idProducto) {
            $this->_idProducto = $_idProducto;
        }

        function get_idEmpresa() {
            return $this->_idEmpresa;
        }

        function set_idEmpresa($_idEmpresa) {
            $this->_idEmpresa = $_idEmpresa;
        }

        function get_idCategoriaPadre() {
            return $this->_idCategoriaPadre;
        }

        function set_idCategoriaPadre($_idCategoriaPadre) {
            $this->_idCategoriaPadre = $_idCategoriaPadre;
        }

        function get_nombre() {
            return $this->_nombre;
        }

        function set_nombre($_nombre) {
            $this->_nombre = $_nombre;
        }
    }
?>