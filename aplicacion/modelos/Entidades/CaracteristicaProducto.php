<?php
    class CaracteristicaProducto {

        private $_idCaracteristica;
        private $_descripcionCaracteristica;
        private $_idProducto;
        private $_rutaImagenCaracteristica;

        function get_idCaracteristica() {
            return $this->_idCaracteristica;
        }

        function set_idCaracteristica($idCaracteristica) {
            $this->_idCaracteristica = $idCaracteristica;
        }

        function get_descripcionCaracteristica() {
            return $this->_descripcionCaracteristica;
        }

        function set_descripcionCaracteristica($descripcionCaracteristica) {
            $this->_descripcionCaracteristica = $descripcionCaracteristica;
        }

        function get_idProducto() {
            return $this->_idproducto;
        }

        function set_idProducto($idproducto) {
            $this->_idproducto = $idproducto;
        }

        function get_rutaImagenCaracteristica() {
            return $this->_rutaImagenCaracteristica;
        }

        function set_rutaImagenCaracteristica($rutaImagenCaracteristica) {
            $this->_rutaImagenCaracteristica = $rutaImagenCaracteristica;
        }
    }
?>