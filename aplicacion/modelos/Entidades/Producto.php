<?php
    class Producto {

        private $_id;
        private $_nombre;
        private $_descripcion;
        private $_codigo;
        private $_imagen;
        private $_idEmpresa;
        private $_caracteristicas;

        function get_id() {
            return $this->_id;
        }

        function set_id($id) {
            $this->_id = $id;
        }

        function get_nombre() {
            return $this->_nombre;
        }

        function set_nombre($nombre) {
            $this->_nombre = $nombre;
        }

        function get_descripcion() {
            return $this->_descripcion;
        }

        function set_descripcion($descripcion) {
            $this->_descripcion = $descripcion;
        }

        function get_codigo() {
            return $this->_codigo;
        }

        function set_codigo($codigo) {
            $this->_codigo = $codigo;
        }

        function get_imagen() {
            return $this->_imagen;
        }

        function set_imagen($imagen) {
            $this->_imagen = $imagen;
        }

        function get_idEmpresa() {
            return $this->_idEmpresa;
        }

        function set_idEmpresa($idEmpresa) {
            $this->_idEmpresa = $idEmpresa;
        }

        function get_caracteristicas() {
            return $this->_caracteristicas;
        }

        function set_caracteristicas($caracteristicas) {
            $this->_caracteristicas = $caracteristicas;
        }
    }
?>