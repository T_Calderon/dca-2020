<?php
    class Empresa {

        private $_idEmpresa;
        private $_NombreEmpresa;
        private $_logoEmpresa;

        function get_idEmpresa() {
            return $this->_idEmpresa;
        }

        function set_idEmpresa($_idEmpresa) {
            $this->_idEmpresa = $_idEmpresa;
        }

        function get_NombreEmpresa() {
            return $this->_NombreEmpresa;
        }

        function set_NombreEmpresa($_NombreEmpresa) {
            $this->_NombreEmpresa = $_NombreEmpresa;
        }

        function get_logoEmpresa() {
            return $this->_logoEmpresa;
        }

        function set_logoEmpresa($_logoEmpresa) { //Por las duda si algo falla, mirar acá. Va sin el guión bajo _
            $this->_logoEmpresa = $_logoEmpresa;
        }
    }
?>