<?php
    require_once BASEPATH . 'biblioteca/DbPdo.php';
    require_once BASEPATH . 'aplicacion/modelos/Entidades/Empresa.php';

    class EmpresaDAO {

      /*
      * _getDbh: Obtiene el método de la conexión a la BD mediante singleton - 03-08-19
      */
      protected function _getDbh() {
          return DbPdo::getInstance()->getConn();
      }

        public function getLogoEmpresa($id_empresa) {

            $conexion = AdministradorDeConexion::getConexion();

            $sql = "SELECT `ruta-logo-empresa` FROM `tblEmpresa` WHERE `id-empresa` = ?";

            $stm = $conexion->prepare($sql);

            $stm->bind_param('i', $id_empresa);

            $stm->execute();

            $stm->bind_result($logoEmpresa);

            $stm->fetch();

            $empresa = new Empresa();

            $empresa->set_IdEmpresa($id_empresa);
            $empresa->set_logoEmpresa($logoEmpresa);

            $stm->close();
            $conexion->close();

            return $empresa;

        }

        public function getNombreEmpresa($id_empresa) {

            //$conexion = AdministradordeConexion::getConexion();
          //  $conexion = $this->_getDbh();

            $sql = "SELECT `nombre-empresa` FROM `tblEmpresa` WHERE `id-empresa` = ?";

            $stm = $this->_getDbh()->prepare($sql);

            $stm->bindParam(1, $id_empresa, PDO::PARAM_INT);

            $stm->execute();

            //$stm->bind_result($nombreEmpresa);

            $stm->fetch();

            $empresa = new Empresa();

            $empresa->set_idEmpresa($id_empresa);

            $empresa->set_NombreEmpresa($nombreEmpresa);

            /*$stm->close();

            $conexion->close();*/

            return $empresa;

        }

        public function getTodasLasEmpresas() {

            $conexion = AdministradordeConexion::getConexion();

            $sql = "SELECT `id-empresa`, `nombre-empresa` FROM `tblEmpresa`";

            $stm = $conexion->prepare($sql);

            $stm->execute();

            $stm->bind_result($idEmpresa, $nombreEmpresa);

            while($stm->fetch()) {

                $empresa = new Empresa();

                $empresa->set_idEmpresa($idEmpresa);

                $empresa->set_NombreEmpresa($nombreEmpresa);

                $empresas[] = $empresa;

            }

            $stm->close();
            $conexion->close();

            return $empresas;

        }
    }
?>
