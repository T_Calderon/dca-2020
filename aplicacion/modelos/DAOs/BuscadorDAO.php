<?php
    require_once '../../biblioteca/DbPdo.php';

    class Buscador {

        /*
        * _getDbh: Obtiene el método de la conexión a la BD mediante singleton - 26-07-19
        */
        protected function _getDbh() {
            return DbPdo::getInstance()->getConn();
        }

        private $conexion=array();

        private $busqueda;

        public function buscar() {

            $conexion = $this->_getDbh();

            //variables para utilizar en la busqueda
            $busqueda = $_GET['s'];

            $query = "SELECT * FROM tblProductos WHERE `nombre-producto` like '%".$busqueda."%' OR `descripcion-producto` like '%".$busqueda."%';";

            //Preparar la consulta
            $resultado = $conexion->prepare($query);

            //$resultado->bindParam(1, $busqueda, PDO::PARAM_STR);
            $resultado->bindValue(1, "%$busqueda%", PDO::PARAM_STR);

            //Ejecutar la Consulta
            $resultado->execute();

            return $resultado->fetchAll(PDO::FETCH_ASSOC);






            //Se cambió el código por que moví todo a ResultadoBuscadorController.php y lo quería así
            /*if (!$resultado->rowCount() == 0) {

                while ($row=$resultado->fetch()){
                   //$this->busqueda[]=$row;
                   echo $row['nombre-producto'];
                }
                //Retornamos los Valores
                //return $this->resultado;
                return $resultado;
            }

            else {
                    echo 'Nothing found';
            }*/


            /* El código de abajo es el viejo - Y funcionaba */
            /*$conexion = AdministradordeConexion;;getConexion();

            $busqueda = mysqli_real_escape_string($conexion, addslashes($_GET['s']));

            $query = "SELECT * FROM tblProductos WHERE `nombre-producto` like '%".$busqueda."%' OR `descripcion-producto` like '%".$busqueda."%';";

            $res = mysqli_query(AdministradordeConexion::getConexion(), $query);

            while($reg = mysqli_fetch_assoc($res)) {

                $this->busqueda[] = $reg;

            }

            return $this->busqueda;*/
        }
    }
?>
