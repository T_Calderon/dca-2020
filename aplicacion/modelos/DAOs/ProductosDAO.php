<?php
    require_once $_SERVER["DOCUMENT_ROOT"] . '/trabajos/DCA/deepcleanargentina_2020/config_dca.ini.php';
    require_once BASEPATH . 'biblioteca/DbPdo.php';
    require_once BASEPATH . 'aplicacion/modelos/Entidades/CaracteristicaProducto.php';
    require_once BASEPATH . 'aplicacion/modelos/Entidades/ImagenProducto.php';

    class ProductosDAO {

        /*
        * _getDbh: Obtiene el método de la conexión a la BD mediante singleton - 03-08-19
        */
        protected function _getDbh() {
            return DbPdo::getInstance()->getConn();
        }

        public final function getProductosCategoria($idCategoria) {

            //$conexion = AdministradorDeConexion::getConexion();

            $sql = 'SELECT tblProductos.`id-producto`,
                           tblProductos.`nombre-producto`,
                           tblProductos.`imagen-producto`,
                           tblProductos.`descripcion-producto`,
                           tblProductos.`codigo-producto`
                    FROM tblProductos_categorias_productos,
                         tblProductos
                    WHERE tblProductos_categorias_productos.`id-producto` = tblProductos.`id-producto`
                    AND `id-categoria-producto` = ?';

            $psProductosCategoria = $conexion->prepare($sql);

            $psProductosCategoria->bind_param('i', $idCategoria);

            $res = $psProductosCategoria->execute();

            $productos = NULL;

            $psProductosCategoria->bind_result($idProducto, $nombreProducto, $imagenProducto, $descripcionProducto, $codigoProducto);

            while($psProductosCategoria->fetch()) {

                $producto = new Producto();

                $producto->set_id($idProducto);
                $producto->set_nombre($nombreProducto);
                $producto->set_descripcion($descripcionProducto);
                $producto->set_imagen($imagenProducto);
                $producto->set_codigo($codigoProducto);

                $caracteristicasDelProducto = $this->getCaracteristicasProducto($idProducto);

                $producto->set_caracteristicas($caracteristicasDelProducto);

                $productos[] = $producto;
            }

            $psProductosCategoria->close();
            $conexion->close();

            return $productos;
        }

        private function getCaracteristicasProducto($idProducto) {

            $conexion = AdministradorDeConexion::getConexion();

            $sql = 'SELECT `id-caracteristica`, `descripcion-caracteristica`, `ruta-imagen-caracteristica`
                    FROM tblCaracterisiticas_productos
                    WHERE `id-producto` = ?';

            $psCaracteristicasProducto = $conexion->prepare($sql);

            $psCaracteristicasProducto->bind_param('i', $idProducto);

            $res = $psCaracteristicasProducto->execute();

            $caracteristicas = NULL;

            $psCaracteristicasProducto->bind_result($idCaracteristica, $descripcionCaracteristica, $rutaImagenCaracteristica);

            while($psCaracteristicasProducto->fetch()) {

                $caracteristica = new CaracteristicaProducto();

                $caracteristica->set_idCaracteristica($idCaracteristica);
                $caracteristica->set_descripcionCaracteristica($descripcionCaracteristica);
                $caracteristica->set_idProducto($idProducto);
                $caracteristica->set_rutaImagenCaracteristica($rutaImagenCaracteristica);

                $caracteristicas[] = $caracteristica;

            }

            $psCaracteristicasProducto->close();
            $conexion->close();

            return $caracteristicas;

        }

        public function guardarProducto($producto, $id_categoria) {

            $conexion = AdministradorDeConexion::getConexion();

            //Guardo el producto
            $sql = 'INSERT INTO tblProducto('
                    . '`nombre-producto`, `descripcion-producto`, `codigo-producto`, `id-empresa`)'
                    . 'VALUES(?, ?, ?, ?)';

            $psProducto = $conexion->prepare($sql);

            $psProducto->bind_param('sssi', $producto->get_nombre(),
                                            $producto->get_descripcion(),
                                            $producto->get_codigo(),
                                            $producto->get_idEmpresa());

            $res = $pdProducto->execute();

            //Lo asocio a una categoría
            $sql = 'INSERT INTO tblProductos_categorias_productos('
                    . '`id-producto`, `id-categoria-producto`)'
                    . 'VALUES(?, ?)';

            $psCategoriaProducto = $conexion->prepare($sql);

            $id_producto = $conexion->insert_id;

            $psCategoriaProducto->bind_param('ii', $id_producto, $id_categoria);

            echo "id prod: " . $conexion->insert_id;
            echo "id cat: " . $id_categoria;

            $res = $psCategoriaProducto->execute();

            echo "Error: " . $psCategoriaProducto->error;

        }

        /**
		* ListarProductos = Metodo para obtener todos los campos de la tabla
        */
        public function ListarProductos() {

            $stm = AdministradorDeConexion::getConexion();

            /* Se crea la declaración de la consulta */
            if($stmt = $stm->prepare('SELECT * FROM tblEmpresas, tblProductos WHERE tblEmpresas.`id-empresa`=tblProductos.`id-empresa`')) {

                /* Ejecutamos la consulta */
                $stmt->execute();

                /* Enlaza las columnas del resultado a variables */
                $stmt->bind_result($rId,$rNombre,$rApellido,$rComentario,$rimagen_producto,$rid_empresa, $hola,$chau);

                /* Creamos el bucle para mostrar el resultado de la consulta */
                while($stmt->fetch()) {

                    echo '<div class="caja-empresa-producto">
                            <dl>
                                <dt>'.$rimagen_producto.'</dt>
                                <dd>Jabón líquido nacarado por 5 litros. Caja por 4 unidades</dd>
                                <dd>Jabón Bactericida para manos por 5 litros sin color ni olor. Aprobado por el SENASA. Caja por 4 unidades</dd>
                                <dd>Alcohol en gel perfumado por 5 litros.</dd>
                            </dl>
                            <img src="http://dummyimage.com/150x150/f4f9e5/0f4f9e" alt="Foto del producto" >
                          </div>';
                }
            }
        }

        //Código modificado para adaptarlo a PDO -- 03-08-19
        public function ListarImagenes($id) {

          return DbPdo::getInstance()->getConn(); //Pongo esto porque sino aparece: Fatal error: Using $this when not in object context in

          //$conexion = $this->_getDbh();

          $sql = 'SELECT tblImagenes_productos.`ruta-imagen`
                  FROM tblProductos, tblImagenes_productos
                  WHERE tblProductos.`id-producto` = tblImagenes_productos.`id-producto`
                  AND tblProductos.`id-producto` = ?';

          $stmt = $this->_getDbh()->prepare($sql);

          if($stmt) {

              $stmt->bind_param('i', $id);
              $stmt->execute();
              $stmt->bind_result($rutaImagen);

                  $imagenes_productos = NULL;

                  while($stmt->fetch()) {

                      $imagen_producto = new ImagenProducto();

                      $imagen_producto->set_rutaImagenProducto($rutaImagen);

                      $imagenes_productos[] = $imagen_producto;

                  }

                  return $imagenes_productos;
          }   else {

              echo "No hay registro";

          }

          /*$stmt->close();
          $conexion->close();*/

            /*return DbPdo::getInstance()->getConn();

            $sql = 'SELECT tblImagenes_productos.`ruta-imagen`
                    FROM tblProductos, tblImagenes_productos
                    WHERE tblProductos.`id-producto` = tblImagenes_productos.`id-producto`
                    AND tblProductos.`id-producto` = ?';

            $stmt = $this->_getDbh()->prepare($sql);

            if($stmt) {

                $stmt->bind_param('i', $id);
                $stmt->execute();
                $stmt->bind_result($rutaImagen);

                    $imagenes_productos = NULL;

                    while($stmt->fetch()) {

                        $imagen_producto = new ImagenProducto();

                        $imagen_producto->set_rutaImagenProducto($rutaImagen);

                        $imagenes_productos[] = $imagen_producto;

                    }

                    return $imagenes_productos;
            }   else {

                echo "No hay registro";

            }

            $stmt->close();
            $conexion->close();*/

        }
    }
?>
