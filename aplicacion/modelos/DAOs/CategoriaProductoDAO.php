<?php
    require_once $_SERVER["DOCUMENT_ROOT"] . '/trabajos/DCA/deepcleanargentina_2020/config_dca.ini.php';
    require_once BASEPATH . 'biblioteca/DbPdo.php';

    class categoriaProductoDAO {

        /*
        * _getDbh: Obtiene el método de la conexión a la BD mediante singleton - 02-08-19
        */
        protected function _getDbh() {
            return DbPdo::getInstance()->getConn();
        }

        private $_idEmpresa;
        private $_idCategoriaPadre;

        public function __construct($idEmpresa, $idCategoriaPadre) {

            $this->_idEmpresa = $idEmpresa;
            $this->_idCategoriaPadre = $idCategoriaPadre;

        }

        public function setIdCategoriaPadre($idCategoriaPadre) {

            $this->_idCategoriaPadre = $idCategoriaPadre;

        }

        public function getCategorias() {

            $conexion = AdministradorDeConexion::getConexion();

            if($this->_idCategoriaPadre == NULL) {

                $sql = 'SELECT * FROM tblCategorias_Productos WHERE `id-empresa` = ? AND `id-categoria-producto-padre` IS NULL';

                $psCategoriasEmpresa = $conexion->prepare($sql);

                $psCategoriasEmpresa->bind_param('i', $this->_idEmpresa);

            } else {

                $sql = 'SELECT * FROM tblCategorias_Productos WHERE `id-empresa` = ? AND `id-categoria-producto-padre` = ?';

                $psCategoriasEmpresa = $conexion->prepare($sql);

                $psCategoriasEmpresa->bind_param('ii', $this->_idEmpresa, $this->_idCategoriaPadre);

            }

            $res = $psCategoriasEmpresa->execute();

            $categorias = NULL;

            $psCategoriasEmpresa->bind_result($idCategoria, $idEmpresa, $idCategoriaPadre, $nombreCategoriaPadre);

            while($psCategoriasEmpresa->fetch()) {

                $CategoriaProducto = new CategoriaProducto();

                $CategoriaProducto->set_nombre($nombreCategoria);
                $CategoriaProducto->set_idProducto($idCategoria);
                $CategoriaProducto->set_idEmpresa($idEmpresa);
                $CategoriaProducto->set_idCategoriaPadre($idCategoriaPadre);

                $categorias[] = $CategoriaProducto;
            }

            $psCategoriasEmpresa->close();
            $conexion->close();

            return $categorias;
        }

        public function guardarCategoria($nombreCategoria) {

            $conexion = AdministradordeConexion::getConexion();

            //Guarda la categoria
            $sql = 'INSERT INTO tblCategorias_producto('
                    . '`id-empresa`, `id-categoria-producto-padre`, `nombre-categoria-producto`)'
                    . 'VALUES(?, ?, ?)';

            $psCategoria = $conexion->prepare($sql);

            $psCategoria->bind_param('iis', $this->_idEmpresa, $this->_idCategoriaPadre, $this->nombreCategoria);

            $res = $psCategoria->execute();

            echo "¡Guardado!";
        }

        public function eliminarCategoria($idCategoria) {

            $conexion = AdministradorDeConexion::getConexion();

            $sql = 'DELETE FROM tblCategorias_productos'
                    . 'WHERE (`id-categoria-producto` = ?)';

            $psCategoria = $conexion->prepare($sql);

            $psCategoria->bind_param('i', $idCategoria);

            $res = $psCategoria->execute();

            echo "¡Eliminado!";
        }

        public function getNombreCategoria($idCategoria) {

            $conexion = AdministradorDeConexion::getConexion();

            $sql = 'SELECT `nombre-categoria-producto` FROM tblCategorias_productos WHERE `id-categoria-producto` = ?';

            $psCategoria = $conexion->prepare($sql);

            $psCategoria->bind_param('i', $idCategoria);

            $res = $psCategoria->execute();

            $psCategoria->bind_result($nombreCategoria);

            $psCategoria->fetch();

            $psCategoria->close();
            $conexion->close();

            return $nombreCategoria;
        }

        //Código modificado para adaptarlo a PDO -- 03-08-19
        public function getCategoriaDelProducto($idProducto) {

            //$conexion = AdministradorDeConexion::getConexion();
            $conexion = $this->_getDbh();

            $sql = 'SELECT `id-categoria-producto` FROM tblProductos_categorias_productos WHERE `id-producto` = ?';

            $psCategoriaProducto = $conexion->prepare($sql);

            //$psCategoriaProducto->bindParam('i', $idProducto);
            $psCategoriaProducto->bindParam(1, $idProducto);

            $res = $psCategoriaProducto->execute();

            //$psCategoriaProducto->bind_result($idCategoriaProducto);

            $psCategoriaProducto->fetch();

            //$psCategoriaProducto->close();
            //$conexion->close();

            //return $idCategoriaProducto;
        }

        public function getCategoriaPadre($idCategoriaPadre) {

            $conexion = AdministradorDeConexion::getConexion();

            $sql = 'SELECT `nombre-categoria-producto`, `id-categoria-producto-padre` FROM tblCategorias_productos WHERE `id-categoria-producto` = ?';

            $psCategoria = $conexion->prepare($sql);

            $psCategoria->bind_param('i', $idCategoriaPadre);

            $res = $psCategoria->execute();

            $categoria = NULL;

            $psCategoria->bind_result($nombreCategoria, $idCategoriaAbuelo);

            $psCategoria->fetch();

            $categoriaProducto = $nombreCategoria;

            $psCategoria->close();
            $conexion->close();

            return $idCategoriaAbuelo;
        }

        public function getBreadCrumbsCategoria($idCategoria) {

            $breadCrumbs[] = $idCategoria;

            while ($idCategoria = $this->getCategoriaPadre($idCategoria)) {

                $breadCrumbs[] = $idCategoria;
            }

            return $breadCrumbs;
        }
    }
?>
