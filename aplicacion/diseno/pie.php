<!-- Footer -->
    <footer class="py-5">
      <div class="container">
          <div class="row">
            <div class="col-lg-4 text-center">
              <h5>Administración y ventas</h5>
              <ul>
                <li>Dirección: Alexis Carrel 3234</li>
              </ul>
            </div>
            <div class="col-lg-4 text-center">
              <h5>Deposito y Logística</h5>
              <ul>
                <li>Dirección: Alexis Carrel 3234</li>
              </ul>
            </div>
            <div class="col-lg-4 text-center">
              <h5>Redes sociales</h5>
              <ul>
                <li><a href="https://www.facebook.com/deepcleanargentina" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a></li>
                <li><a href="https://twitter.com/DcaArgentina" target="_blank"><i class="fab fa-twitter-square fa-2x"></i></a></li>
                <li><a href="https://www.instagram.com/deepcleanargentina/" target="_blank"><i class="fab fa-instagram fa-2x"></i></a></li>
                <li><a href="https://www.linkedin.com/company/deepcleanargentina" target="_blank"><i class="fab fa-linkedin fa-2x"></i></a></li>
              </ul>
            </div>
          </div>        

          <hr>

          <div class="row pie-da">
            <div class="col-lg-12">
              <p class="m-0 text-right text-white">Sitio desarrollado por:
                <!-- Popover #1 -->
                <a href="https://www.diseñoatsui.com/" class="pop" target="_blank"  data-container="body" data-toggle="popover" data-placement="top" data-content="Para más información sobre otros proyectos de <b>Diseño Atsui</b> se lo invita a que visite:<br><a href=&quot;https://www.xn--diseoatsui-w9a.com/&quot; target=&quot;_blank&quot;>www.disenioatsui.com.ar</a> <img src=&quot;<?php echo BASEURL;?>/publico/img/pie/logo_d_a_tooltip.png&quot;> <br>Muchas gracias por su atención."
                    data-original-title="" title="">
                      Diseño Atsui
                </a>
              </p>
 

            </div>
          </div>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo BASEURL;?>/publico/js/jquery.min.js"></script>
    <script src="<?php echo BASEURL;?>/publico/js/bootstrap.bundle.min.js"></script>

    <!-- Popover -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <!-- Initialize Bootstrap functionality -->
    <script>
      $(".pop").popover({ trigger: "manual" , html: true, animation:false})
          .on("mouseenter", function () {
              var _this = this;
              $(this).popover("show");
              $(".popover").on("mouseleave", function () {
                  $(_this).popover('hide');
              });
          }).on("mouseleave", function () {
              var _this = this;
              setTimeout(function () {
                  if (!$(".popover:hover").length) {
                      $(_this).popover("hide");
                  }
              }, 300);
        });

      // Para el formulario de contacto. Validaciones personalizadas.
      var email = document.getElementById("correoelectronico");

      email.addEventListener("keyup", function (event) {
        if (email.validity.typeMismatch) {
          email.setCustomValidity("Tiene que ser una dirección de correo electrónico válida");
        } else {
          email.setCustomValidity("");
        }
      });

      // Para el contador de caracteres del textarea
      var maxchars = 255;

      $('textarea').keyup(function () {
          var tlength = $(this).val().length;
          $(this).val($(this).val().substring(0, maxchars));
          var tlength = $(this).val().length;
          remain = maxchars - parseInt(tlength);
          $('#restantes').text(remain);
      });
      </script>

      <!-- Código para la galería de fotos -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
      <script>
          baguetteBox.run('.tz-gallery');
      </script>

  </body>

</html>