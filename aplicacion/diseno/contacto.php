<?php include_once '../../aplicacion/diseno/encabezado.php'; ?>

    <!-- Page Content -->
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo BASEURL . 'index.php'; ?>">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contacto</li>
            </ol>
        </nav>

      <!-- Portfolio Item Heading -->
      <h4 class="my-4">Contacto</h4>

      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-4 direccion">
            <ul>
                <li>
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-map-marker-alt fa-stack-1x fa-inverse"></i>
                    </span> Alexis Carrel 3234
                </li>
                <li>
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-at fa-stack-1x fa-inverse"></i>
                    </span> info@deepcleanargentina.com
                </li>
                <li>
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-mobile-alt fa-stack-1x fa-inverse"></i>
                    </span> (011) 4736-8473
                </li>
                <li>
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                    </span> facebook.com/deepcleanargentina
                </li>
                <li>
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                    </span> twitter.com/DcaArgentina
                </li>
                <li>
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
                    </span> instagram.com/deepcleanargentina
                </li>
                <li>
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fab fa-linkedin-in fa-stack-1x fa-inverse"></i>
                    </span> linkedin.com/company/deepcleanargentina
                </li>
            </ul>
        </div>

        <div class="col-md-8">
            <form id="formulario-contacto" name="EnviarMensaje" action="../../aplicacion/controladores/ContactoController.php" method="post">
                <fieldset class="form-group">
                    <legend>Envíenos su consulta</legend>
                    <p>Le responderemos a la mayor brevedad posible. Muchas Gracias.</p>

                    <div class="form-group">
                        <!-- <label for="example-text-input">Text</label> -->
                        <input name="Nombre" class="form-control" type="text" id="nombre" placeholder="Nombre" required>
                    </div>
                    <div class="form-group">
                        <!-- <label for="example-tel-input" class="col-2 col-form-label">Telephone</label> -->
                        <input name="Telefono" class="form-control" type="tel" id="example-tel-input" placeholder="Número de teléfono" required>
                    </div>
                    <div class="form-group">
                        <!-- <label for="exampleInputEmail1">Email address</label> -->
                        <input name="CorreoElectronico" type="email" class="form-control" id="correoelectronico" aria-describedby="emailHelp" placeholder="Correo Electrónico" required>
                        <small id="emailHelp" class="form-text text-muted">Nunca compartiremos su correo electrónico con nadie más.</small>
                    </div>
                    <div class="form-group">
                        <!-- <label for="exampleTextarea">Example textarea</label> -->
                        <textarea name="Mensaje" class="form-control" id="exampleTextarea" rows="3" maxlength="255" placeholder="Mensaje" required></textarea>
                        <small id="contadorHelp" class="form-text text-muted"><span id="restantes">255</span> caracteres restantes</small>
                    </div>
                </fieldset>
                <p>[recaptcha]</p>
                <input type="hidden" name="Enviar" value="contacto">
                <button type="submit" class="btn btn-primary">Enviar mensaje</button>
            </form>
        </div>

      </div>
      <!-- /.row -->

      <div class="row mapa">
        <div class="col-lg-12">
            <div class="google-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3289.578506493602!2d-58.64830288432425!3d-34.462845857565966!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bca37d58279211%3A0x8dfdc0b933b38ea!2sEl+Talar!5e0!3m2!1ses!2sar!4v1547331393058"></iframe>
            </div>
        </div>
      </div>
      <!-- /.row Maps -->

      <div class="row contacto-inferior">
        <div class="col-lg-4 text-center dca-logo-texto">
            <img src="<?php echo BASEURL;?>publico/img/contacto/logo_deepcleanargentina.png" alt="Logo Deep Clean Argentina">
            <p>Asesoría Integral en Sistemas de Higiene</p>
        </div>

        <div class="col-lg-8 info-extra">
            <h5>CONTACTO</h5>
            <hr>

            <div class="row">
                <div class="col-lg-4">
                    <h6>Administración y ventas</h6>
                        <p>Dirección: Alexis Carrel 3234.</p>

                    <h6>Deposito y Logística</h6>
                        <p>Dirección: Alexis Carrel 3234.</p>
                </div>
                <div class="col-lg-4">
                    <p>info@deepcleanargentina.com</p>
                </div>
                <div class="col-lg-4">
                    <p>(011) 4736-8473</p>
                </div>
            </div>

        </div>
      </div>
      <!-- /.row Contacto Inferior -->

    </div>
    <!-- /.container -->

<?php include_once '../../aplicacion/diseno/pie.php'; ?>
