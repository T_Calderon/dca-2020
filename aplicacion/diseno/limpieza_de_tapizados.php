<?php include_once '../../aplicacion/diseno/encabezado.php'; ?>

    <!-- Page Content -->
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo BASEURL . 'index.php'; ?>">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Limpieza de tapizados</li>
            </ol>
        </nav>

      <!-- Portfolio Item Heading -->
      <h4 class="my-4">Limpieza de tapizados</h4>

      <!-- Portfolio Item Row -->
      <div class="row tapizado-texto">
        <div class="col-lg-12">
          <p>Deep Clean Argentina confía en la mano de obra especializada de <b>SJ Expertos</b> para la limpieza de Tapizados y alfombras.</p>
        </div>
      </div>
      <!-- /.row -->

        <div class="tz-gallery">
            <div class="row text-center text-lg-left tapizado-imagenes">

                <div class="col-lg-3 col-md-4 col-xs-6">
                    <a href="<?php echo BASEURL;?>publico/img/sj_limpieza/sj_limpieza_1_n.jpg" class="d-block mb-4 h-100 lightbox">
                        <img class="rounded-circle img-fluid img-thumbnail" src="<?php echo BASEURL;?>publico/img/sj_limpieza/miniaturas/sj_limpieza_1_n.png" alt="Ejemplo de limpieza">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-6">
                    <a href="<?php echo BASEURL;?>publico/img/sj_limpieza/sj_limpieza_2_o.jpg" class="d-block mb-4 h-100 lightbox">
                        <img class="rounded-circle img-fluid img-thumbnail" src="<?php echo BASEURL;?>publico/img/sj_limpieza/miniaturas/sj_limpieza_2_o.png" alt="Ejemplo de limpieza">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-6">
                    <a href="<?php echo BASEURL;?>publico/img/sj_limpieza/sj_limpieza_3_o.jpg" class="d-block mb-4 h-100 lightbox">
                        <img class="rounded-circle img-fluid img-thumbnail" src="<?php echo BASEURL;?>publico/img/sj_limpieza/miniaturas/sj_limpieza_3_o.png" alt="Ejemplo de limpieza">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-6">
                    <a href="<?php echo BASEURL;?>publico/img/sj_limpieza/sj_limpieza_n.jpg" class="d-block mb-4 h-100 lightbox">
                        <img class="rounded-circle img-fluid img-thumbnail" src="<?php echo BASEURL;?>publico/img/sj_limpieza/miniaturas/sj_limpieza_n.png" alt="Ejemplo de limpieza">
                    </a>
                </div>

            </div>
        </div>

    </div>
    <!-- /.container -->

<?php include_once '../../aplicacion/diseno/pie.php'; ?>
