<?php include_once '../../aplicacion/diseno/encabezado.php'; ?>

    <!-- Page Content -->
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo BASEURL . 'index.php'; ?>">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Quiénes somos</li>
            </ol>
        </nav>

      <!-- Portfolio Item Heading -->
      <h4 class="my-4">Quiénes somos</h4>

      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-4">
          <img class="img-fluid quienes-somos" src="<?php echo BASEURL;?>publico/img/quienes_somos/miniatura/limpieza.jpeg" alt="Productos de limpieza">
        </div>

        <div class="col-md-8">
          <h5 class="my-3">Deep Clean Argentina</h5>
            <p>Es una Compañía dedicada a brindar soluciones integrales en Sistemas de Higiene para Empresas.  Entendemos nuestro negocio con una visión amplia, donde no solo fabricamos y comercializamos productos propios, sino que entendemos las necesidades puntuales de cada Cliente y ofrecemos una solución a medida.</p> 
			<p>Somos una empresa que evolucionó en el tiempo, pasando de ser solo fabricantes y distribuidores, a brindar un servicio de Asesoria Integral, transformándose éste último en nuestro core business.</p> 
            <p>Concebimos a cada uno de nuestros clientes como &Uacute;NICOS. Esto nos permitió crecer en base a la confianza que cada uno de nuestros clientes depositó en DCA.</p>
        </div>

      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->

<?php include_once '../../aplicacion/diseno/pie.php'; ?>
