<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/trabajos/DCA/deepcleanargentina_2020/config_dca.ini.php'; //Local
  require_once BASEPATH . 'biblioteca/Ayudantes.php';
?>
<!DOCTYPE html>
<html lang="es">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Deep Clean Argentina - <?php echo Ayudantes::getTituloPagina(); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASEURL;?>/publico/css/bootstrap.min.css" rel="stylesheet">
    <!-- CSS Propio -->
    <link href="<?php echo BASEURL;?>/publico/css/estilo_css.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo BASEURL;?>/publico/fuentes/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet"> <!--load all styles -->
    <!-- Custom styles for this template -->
    <link href="<?php echo BASEURL;?>/publico/css/modern-business.css" rel="stylesheet">
    <!-- Estilo para galería de imágenes -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">

  </head>

  <body>

    <div class="container">
      <div class="row dca-copete">
        <div class="col-lg-4 text-center">
            <img src="<?php echo BASEURL;?>/publico/img/encabezado/logo_dca_57x100.png" alt="Logo Deep Clean Argentina">
        </div>

        <div class="col-lg-8 text-center">
          <h1>Deep Clean Argentina</h1>
          <p>Asesoría Integral en Sistemas de Higiene</p>
        </div>
      </div>
    </div>


    <?php include_once 'barra_navegacion.inc'; ?>
