<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark <!--bg-dark--> static-top">
      <div class="container">
        <!-- <a class="navbar-brand" href="#">Start Bootstrap</a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">

            <!-- <li class="nav-item active"> -->
            <li class="nav-item">
              <a class="nav-link" href="<?php echo BASEURL;?>index.php">Inicio
                <span class="sr-only">(current)</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="<?php echo BASEURL;?>aplicacion/diseno/quienes_somos.php">Quiénes somos</a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Catalogo de productos
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Productos Italimpia</a>
                <a class="dropdown-item" href="#">Productos Sutter</a>
                <a class="dropdown-item" href="#">Productos Alani</a>
                <a class="dropdown-item" href="#">Productos Kärcher</a>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="<?php echo BASEURL;?>aplicacion/diseno/limpieza_de_tapizados.php">Limpieza de tapizados</a>
            </li>

            <!-- <li class="nav-item">
              <a class="nav-link" href="#">Newsletter</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">RR HH</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Proveedores</a>
            </li> -->

            <li class="nav-item">
              <a class="nav-link" href="<?php echo BASEURL;?>aplicacion/diseno/contacto.php">Contacto</a>
            </li>

          </ul>
          <form name="form" class="form-inline my-2 my-lg-0" action="<?php echo BASEURL; ?>aplicacion/controladores/ResultadoBuscadorController.php" method="GET">
            <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Search" name="s" required>
            <button class="btn btn-outline-success my-2 my-sm-0" value="Buscar" type="submit"><i class="fas fa-search"></i></button>
          </form>
        </div>
      </div>
    </nav>
