<?php
  include_once '../../aplicacion/diseno/encabezado.php';

  require_once '../modelos/DAOs/CategoriaProductoDAO.php';
  require_once '../modelos/DAOs/ProductosDAO.php';

  require_once '../modelos/Entidades/categoriaProducto.php';
  require_once '../modelos/Entidades/Producto.php';

  require_once '../modelos/Entidades/Empresa.php';
  require_once '../modelos/DAOs/EmpresaDAO.php';

  require_once '../modelos/DAOs/EmpresaDAO.php';

    function EmpresaDAO($id_empresa) {

      $administradorDeEmpresas = new EmpresaDAO();

      $NombreEmpresa = $administradorDeEmpresas->getNombreEmpresa($id_empresa);

      $nombre = $NombreEmpresa->get_NombreEmpresa();

      return $nombre;

    function generarMainMenu() {

      $administradorDeEmpresas = new EmpresaDAO();

      $empresas = $administradorDeEmpresas->getTodasLasEmpresas();

      echo '<select>';

        foreach ($empresas as $empresa) {
          echo '<option>' . $empresa->get_NombreEmpresa() . '</option>';

        }

        echo '</select>';

    }
  }
?>

    <!-- Page Content -->
    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo BASEURL . 'index.php'; ?>">Inicio</a></li>

                <?php

                  $administradorEmpresas = new EmpresaDAO();

                  $empresa = $administradorEmpresas->getNombreEmpresa($_GET['empresa']);
                  
                ?>
                <li class="breadcrumb-item active" aria-current="page">Productos xxxxxxx</li>

            </ol>
        </nav>

      <!-- Portfolio Item Heading -->
      <h4 class="my-4">Productos xxxxxxx</h4>

      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-lg-9  order-lg-1">



            <img class="img-fluid d-block mx-auto logo-en-catalogo" src="http://placehold.it/266x166" alt="Logo Empresa">



            <!-- <div class="col-lg-12 productos">
                <div class="row">
                    <div class="col-8 col-sm-6">

                        <div class="card-body">
                        <h4 class="card-title">
                            <a href="#">Código: ITA018000</a>
                        </h4>
                        <h5>Escurridores a libro</h5>
                            <p class="card-text">Resistente y de alta eficiencia.</p>
                            <p class="card-text">Reduce el esfuerzo debido a la manija curva, más presión con menos esfuerzo.</p>
                            <a href="#" class="btn btn-info" role="button" aria-disabled="true">Ver más información</a>
                        </div>

                    </div>
                    <div class="col-4 col-sm-6">
                        <a href="#"><img class="card-img-top" src="http://placehold.it/266x166" alt="Imagen del producto"></a>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 productos">
                <div class="row">
                    <div class="col-8 col-sm-6">

                        <div class="card-body">
                        <h4 class="card-title">
                            <a href="#">Código: ITA018000</a>
                        </h4>
                        <h5>Escurridores a libro</h5>
                            <p class="card-text">Resistente y de alta eficiencia.</p>
                            <p class="card-text">Reduce el esfuerzo debido a la manija curva, más presión con menos esfuerzo.</p>
                            <a href="#" class="btn btn-info" role="button" aria-disabled="true">Ver más información</a>
                        </div>

                    </div>
                    <div class="col-4 col-sm-6">
                        <a href="#"><img class="card-img-top" src="http://placehold.it/266x166" alt="Imagen del producto"></a>
                    </div>
                </div>
            </div> -->



            <!-- Código para ver las características del producto -->
            <!-- <div class="card">
                <img class="card-img-top img-fluid" src="http://placehold.it/900x400" alt="Imagen del producto">
                <div class="card-body">
                <h3 class="card-title">Product Name</h3>
                <h4>$24.99</h4>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dicta fugit fugiat hic aliquam itaque facere, soluta. Totam id dolores, sint aperiam sequi pariatur praesentium animi perspiciatis molestias iure, ducimus!</p>
                <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
                4.0 stars
                </div>
            </div> -->
            <!-- /.card -->

        </div>
        <!-- /.col-lg-9 -->

        <div class="col-lg-3 order-lg-0">

            <ul class="list-group catalogo">
                <li class="list-group-item">Baldes de limpieza
                    <ul>
                        <li>Baldes</li>
                        <li>Baldes prensamopas</li>
                        <li>Escurridores</li>
                    </ul>
                </li>
                <li class="list-group-item">Baños</li>
                <li class="list-group-item">Carros
                    <ul>
                        <li>Hotelería y lavandería</li>
                        <li>Limpieza general</li>
                        <li>Limpieza hospitalaria</li>
                        <li>Transporte</li>
                        <li>Transporte hospitalario</li>
                    </ul>
                </li>
                <li class="list-group-item">Porta ac consectetur ac</li>
                <li class="list-group-item">Vestibulum at eros</li>
            </ul>

        </div>
        <!-- /.col-lg-3 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include_once '../../aplicacion/diseno/pie.php'; ?>
