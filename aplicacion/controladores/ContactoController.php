<?php
    //print_r($_POST);
    require_once $_SERVER['DOCUMENT_ROOT'] . '/trabajos/DCA/deepcleanargentina_2020/config_dca.ini.php';
    require_once BASEPATH . 'biblioteca/Inputfilter.php';
    require_once BASEPATH . 'biblioteca/Ayudantes.php';

    //Así es como debes incluir la librería
    require_once BASEPATH . 'biblioteca/recaptchalib.php';

    //Asignas la clave secreta del paso 4 a una variable llamada $claveSecreta
    $claveSecreta = "6LfbdSgUAAAAAHzgr1BREfgJ8f1CkhEH6gv7BNNz";
    // Por default asumimos que tenemos una respuesta vacía
    $respuesta = null;
    //Creamos una instancia de la clase que incluimos
    $captcha = new ReCaptcha($claveSecreta);

    //Validamos que si hayamos recibido por post la respuesta que recibimos de google al validar el captcha
    if ($_POST["g-recaptcha-response"])
    {
        //Le pedimod a google que valide la respuesta del captcha; para
        //ello enviamos nuestro dominio y la respuesta
        $respuesta = $captcha ->verifyResponse
        (
        "gbcomputacion.com.ar",
        $_POST["g-recaptcha-response"]
        );
    }

    //Si la respuesta que obtenemos de la comprobación es diferente a null y además es exitosa...
    //...listo! todo está en orden y entonces podemos enviar el correo electrónico
    if ($respuesta != null && $respuesta ->success)
    {
        $obj_clean = new InputFilter();

        $nombre = $obj_clean->process($_POST['Nombre']);
        $telefono = $obj_clean->process($_POST['Telefono']);
        $correoelectronico = $obj_clean->process($_POST['CorreoElectronico']);        
        $mensaje = $obj_clean->process($_POST['Mensaje']);

        if (isset($_POST['Enviar'])) {

        // Debes editar las próximas dos líneas de código de acuerdo con tus preferencias
        $email_to = "gustavo@gbcomputacion.com.ar";
        $email_subject = "Formulario web";

        if (!isset($nombre) ||
            !isset($correoelectronico) ||
            !isset($telefono) ||
            !isset($mensaje)) {

            echo "<b>Ocurrió un error y el formulario no ha sido enviado. </b><br >";
            echo "Por favor, vuelva al formulario y verifique la información ingresada<br >";
            die();

        }

        $email_message = "Detalles del formulario de contacto:\n\n";
        $email_message .= "Nombre: " . $nombre . "\n";
        $email_message .= "Correo Electrónico: " . $correoelectronico . "\n";
        $email_message .= "Teléfono: " . $telefono . "\n";
        $email_message .= "Mensaje: " . $mensaje . "\n\n";

        // Ahora se envía el e-mail usando la función mail() de PHP
        $headers = 'From: '.$correoelectronico."\r\n" .
        'Reply-To: '.$correoelectronico."\r\n" .
        'X-Mailer: PHP/' . phpversion();
        @mail($email_to, $email_subject, $email_message, $headers);

        include_once '../diseno/encabezado.php';
        echo '<!-- Page Content -->
                <div class="container">

                    <!-- Portfolio Item Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="text-center envio-exitoso">¡El formulario se ha enviado con éxito!</h1>
                        </div>
                    </div>
                    <!-- /.row -->

                </div>';
        include_once '../diseno/pie.php';
        }
    }
    else
    {
        //Programar aquí alguna acción para indicarle al usuario que la validación del captcha fue incorrecta
        include_once '../diseno/encabezado.php';
        echo '<!-- Page Content -->
            <div class="container">

                <!-- Portfolio Item Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="text-center envio-exitoso">Algo falló con la validación. Vamos de nuevo <a href="http://gbcomputacion.com.ar/index.php#contact">Formulario</a></h1>
                    </div>
                </div>
                <!-- /.row -->

            </div>';
        include_once '../diseno/pie.php';
    }
?>