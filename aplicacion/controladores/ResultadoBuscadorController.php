<?php
    require_once("../../aplicacion/modelos/DAOs/BuscadorDAO.php");

    include_once '../../aplicacion/diseno/encabezado.php';

    require_once '../modelos/DAOs/CategoriaProductoDAO.php';
    require_once '../modelos/DAOs/ProductosDAO.php';

    require_once '../modelos/Entidades/categoriaProducto.php';
    require_once '../modelos/Entidades/Producto.php';

    require_once '../modelos/Entidades/Empresa.php';
    require_once '../modelos/DAOs/EmpresaDAO.php';
?>

<table class="table">
  <tr>
    <th>Nombre del producto</th>
    <th>Descripción del producto</th>
    <th>Imagen</th>
  </tr>

<?php
//COMPROBAMOS SI HAY REGISTROS EN LA BUSQUEDA, SI NO LOS HAY, MOSATRAMOS UN MENSAJE DICIENDO QUE NO HAY RESULTADOS, EN OTRO CASO, MOSTRAMOS LOS RESULTADOS
$bus = new Buscador();
$buscame = $bus->buscar();

    if (count($buscame) == 0) {

      echo 'Search again here<br/>';

    } else {

      for ($i=0; $i < sizeof($buscame); $i++) {

?>
  <tr>
    <td>

      <?php

        $administradorDeCategorias = new categoriaProductoDAO(NULL, NULL);

        $categoriaDelProducto = $administradorDeCategorias->getCategoriaDelProducto($buscame[$i]["id-producto"]);

        //echo $buscame[$i]["nombre-producto"];
        echo '<a href="'. BASEURL .'aplicacion/diseno/catalogo.php?empresa='.$buscame[$i]['id-empresa'].'&amp;producto='.$categoriaDelProducto.'#'.$buscame[$i]["id-producto"].'">
              '.$buscame[$i]["nombre-producto"].'
              </a>';

      ?>

    </td>
    <td>

      <?php echo $buscame[$i]["descripcion-producto"]; ?>

    </td>
    <td>

      <?php

        $administradorDeCategorias = new categoriaProductoDAO(NULL, NULL);

        $categoriaDelProducto = $administradorDeCategorias->getCategoriaDelProducto($buscame[$i]["id-producto"]);

        //Código para obtener la imagen del producto
        //Cargo la imagen
        $imagenes_producto = ProductosDAO::ListarImagenes($buscame[$i]["id-producto"]);

        if ($imagenes_producto) {

          echo '<a href="'. BASEURL .'aplicacion/diseno/catalogo.php?empresa='.$buscame[$i]['id-empresa'].'&amp;producto='.$categoriaDelProducto.'#'.$buscame[$i]["id-producto"].'">
                  <img src="'.$imagenes_productos[0]->get_rutaImagenProducto().'" alt="Foto del producto" width="200"/>
                </a>';

        }

      ?>

    </td>
  </tr>
<?php

      }
    }
?>
</table>

<!--    /*if (isset($_GET['s'])) {
      //$s = $_GET['s'];

      $bus = new Buscador();
      $buscame = $bus->buscar();

      $iterator = new ArrayIterator($buscame);

      foreach ($iterator as $row) {
        echo $row['id-empresa'] . $row['nombre-producto'] . '<br/>';
      }

    } else {
      echo 'Search again here<br/>';
    }*/
-->

<?php include_once '../../aplicacion/diseno/pie.php'; ?>
