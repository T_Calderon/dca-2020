<?php include_once 'aplicacion/diseno/encabezado.php'; ?>
<header>
        <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>

            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active" style="background-image: url('<?php echo BASEURL;?>publico/img/portada/carrusel_principal_adaptado.jpg')">
              <!-- <div class="carousel-caption d-none d-md-block">
                <h2>First Slide</h2>
                <p>This is a description for the first slide.</p>
              </div> -->
            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('<?php echo BASEURL;?>publico/img/portada/dca_slider_2a_2020.jpg')">
              <div class="carousel-caption d-none d-md-block">
                <h2>Second Slide</h2>
                <p>This is a description for the second slide.</p>
              </div>
            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('<?php echo BASEURL;?>publico/img/portada/dca_slider_3a_2020.jpg')">
              <div class="carousel-caption d-none d-md-block">
                <h2>Third Slide</h2>
                <p>This is a description for the third slide.</p>
              </div>
            </div>

            <!-- Slide Four - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('<?php echo BASEURL;?>publico/img/portada/dca_slider_4a_2020.jpg')">
              <div class="carousel-caption d-none d-md-block">
                <h2>Four Slide</h2>
                <p>This is a description for the third slide.</p>
              </div>
            </div>
            <!-- Slide Five - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('<?php echo BASEURL;?>publico/img/portada/dca_slider_5a_2020.jpg')">
              <div class="carousel-caption d-none d-md-block">
                <h2>Five Slide</h2>
                <p>This is a description for the third slide.</p>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </header>

    <!-- Page Content -->
    <div class="container">
      <div class="row">

        <div class="col-lg-6">
          <h3 class="mt-5">Bienvenidos a DEEP CLEAN ARGENTINA</h3>
            <p>En nosotros encontrará un servicio profesional de asesoría integral en Sistemas de Higiene para su Empresa.</p>
            <p>Nuestra combinación de productos diseñados y pensados para su satisfacción junto a un Staff de colaboradores altamente capacitados, nos permite atender de manera eficiente a cada uno de nuestros clientes.</p>
            <p>Los invitamos a conocer más sobre nosotros.</p>
            <img class="img-fluid" src="http://www.unv.cl/src/img/gallery/big/aseo/1.jpg" alt="Imagen ilustrativa">
        </div>

        <div class="col-lg-6">
          <h3 class="mt-5">Empresas con las que trabajamos</h3>

          <!-- Project One -->
          <div class="row">
            <div class="col-md-4">
                <img class="img-fluid rounded mb-3 mb-md-0" src="<?php echo BASEURL;?>publico/img/italimpia/logo_italimpia_1.png" alt="Logo de Italimpia">
            </div>
            <div class="col-md-8">
                <p><b>Italimpia</b>: Lideres en fabricación de insumos <b>P</b>lásticos para limpieza de hogares, comercios e industrias.</p>
            </div>
          </div>
          <!-- /.row -->

          <hr>

          <!-- Project Two -->
          <div class="row">
            <div class="col-md-4">
                <img class="img-fluid rounded mb-3 mb-md-0" src="<?php echo BASEURL;?>publico/img/alani/logo_alani_1.png" alt="Logo de Alaní">
            </div>
            <div class="col-md-8">
                <p><b>Alani</b>: Empresa dedicada a la fabricación de todo tipo de papeles para la <b>H</b>igiene y la <b>L</b>impieza.</p>
            </div>
          </div>
          <!-- /.row -->

          <hr>

          <!-- Project Three -->
          <div class="row">
            <div class="col-md-4">
                <img class="img-fluid rounded mb-3 mb-md-0" src="<?php echo BASEURL;?>publico/img/sutter/logo_sutter_1.png" alt="Logo de Sutter">
            </div>
            <div class="col-md-8">
                <p><b>Sutter Profesional</b>: Empresa multinacional fabricante de productos <b>Q</b>uímicos orientados al sector profesional, gastronómico, hospitalario e industria.</p>
            </div>
          </div>
          <!-- /.row -->

          <hr>

          <!-- Project Four -->
          <div class="row">

            <div class="col-md-4">
                <img class="img-fluid rounded mb-3 mb-md-0" src="<?php echo BASEURL;?>publico/img/karcher/logo_karcher_1.jpg" alt="Logo de Kärcher">
            </div>
            <div class="col-md-8">
                <p><b>Kärcher</b>: Empresa familiar alemana, líder mundial en soluciones de limpieza que agregan valor y ahorran tiempo.</p>
            </div>
          </div>
          <!-- /.row -->

          <hr>

        </div>

      </div>
    </div>
<?php include_once 'aplicacion/diseno/pie.php'; ?>
